package com.testaps.hindibible.versus

class CustomException(message: String) : Exception(message)
